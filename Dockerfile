FROM openjdk:8-jre
COPY target /bookservice
WORKDIR /bookservice
ENTRYPOINT ["java", "-jar", "bookservicerest-0.0.1-SNAPSHOT.jar"]