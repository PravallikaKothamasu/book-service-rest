package com.epam.bookservice.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.stereotype.Component;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Component
public class Book {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(notes = "Id of the Boook", name = "bookId", value = "test bookId")
	int bookId;
	@ApiModelProperty(notes = "title of the book", name = "title", required = true, value = "test title")
	String title;
	@ApiModelProperty(notes = "author of the book", name="author",required = true,value = "test author")
	String author;
	@ApiModelProperty(notes = "genre of the book", name = "genre", required = true, value = "test genre")
	String genre;
	@ApiModelProperty(notes = "cost of the book",name = "cost",required = true,value = "test cost")
	Long cost;

	public Book(int bookId, String title, String author, String genre, Long cost) {
		super();
		this.bookId = bookId;
		this.title = title;
		this.author = author;
		this.genre = genre;
		this.cost = cost;
	}

	public Book() {

	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public Long getCost() {
		return cost;
	}

	public void setCost(Long cost) {
		this.cost = cost;
	}

}
