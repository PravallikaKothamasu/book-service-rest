package com.epam.bookservice.model;

import io.swagger.annotations.ApiModelProperty;

public class BookDTO {
	@ApiModelProperty(notes = "Id of the BookDTO", name = "bookId", value = "test bookId")
	int bookId;
	@ApiModelProperty(notes = "title of the BookDTO", name = "title", required = true, value = "test title")
	String title;
	@ApiModelProperty(notes = "author of the BookDTO", name="author",required = true,value = "test author")
	String author;
	@ApiModelProperty(notes = "genre of the BookDTO", name = "genre", required = true, value = "test genre")
	String genre;
	@ApiModelProperty(notes = "cost of the BookDTO",name = "cost",required = true,value = "test cost")
	Long cost;
	public BookDTO(int bookId, String title, String author, String genre, Long cost) {
		super();
		this.bookId = bookId;
		this.title = title;
		this.author = author;
		this.genre = genre;
		this.cost = cost;
	}
	public BookDTO() {
	}
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public Long getCost() {
		return cost;
	}
	public void setCost(Long cost) {
		this.cost = cost;
	}
	@Override
	public String toString() {
		return "BookDTO [bookId=" + bookId + ", title=" + title + ", author=" + author + ", genre=" + genre + ", cost="
				+ cost + "]";
	}
	
	
	
}
