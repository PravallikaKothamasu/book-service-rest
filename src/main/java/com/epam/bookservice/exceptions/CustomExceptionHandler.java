package com.epam.bookservice.exceptions;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class CustomExceptionHandler {
	
	@ExceptionHandler(NoBooksException.class)
	public final ResponseEntity<ErrorResponse> handleNoBooksException(NoBooksException ex, WebRequest request)
	{
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse("INCORRECT_REQUEST", details);
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(NoSuchBookFound.class)
	public final ResponseEntity<ErrorResponse> handleNoSuchBookFoundException(NoSuchBookFound ex, WebRequest request)
	{
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse("INCORRECT_REQUEST", details);
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}

}
