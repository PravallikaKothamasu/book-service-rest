package com.epam.bookservice.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.epam.bookservice.model.Book;
import com.epam.bookservice.model.BookDTO;
import com.epam.bookservice.service.BookService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/books")
public class BookController {

	@Autowired
	BookService bookService;
	
	private static final Logger LOG = LogManager.getLogger(BookController.class);

	@ApiOperation(value = "Returns All Books")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieves all books") })
	@GetMapping
	public ResponseEntity<List<Book>> getListOfAllBooks() {
		LOG.info("inside get books: /books");
		List<Book> booksList = bookService.findAllBooks();
		return ResponseEntity.ok(booksList);
	}

	@ApiOperation(value = "Returns a book by given bookId")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrives a book by given bookId"),
			@ApiResponse(code = 404, message = "Book not found with given book id") })
	@GetMapping("{bookId}")
	public ResponseEntity<Book> getBookById(
			@ApiParam(value = "BookId Id for which the book will be retrieved", required = true) @PathVariable int bookId) {
		LOG.info("inside get book: /books/bookId");
		Book selectedBook = bookService.getBookById(bookId);
		return ResponseEntity.ok(selectedBook);
	}

	@ApiOperation(value = "Adds a Book")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully book is created and added"), })
	@PostMapping
	public ResponseEntity<Book> addBook(
			@ApiParam(value = "Book that is to be added", required = true) @RequestBody BookDTO bookToBeAdded) {
		LOG.info("inside add a book: /books");
		Book savedBook = bookService.addBook(bookToBeAdded);
		final java.net.URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{bookId}")
				.buildAndExpand(savedBook.getBookId()).toUri();
		return ResponseEntity.created(uri).body(savedBook);
	}

	@ApiOperation(value = "Deletes a book by given bookId")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully deletes a book by given bookId"),
			@ApiResponse(code = 404, message = "Book not found with given book id") })
	@DeleteMapping("{bookId}")
	public ResponseEntity<Void> deleteBookById(
			@ApiParam(value = "Book that is to be deleted", required = true) @PathVariable int bookId) {
		LOG.info("inside delete a book /books/bookId");
		bookService.deleteBookById(bookId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@ApiOperation(value = "Updates a book with given booId")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully updates a book by given bookId"),
			@ApiResponse(code = 404, message = "Book not found with given book id") })
	@PutMapping("{bookId}")
	public ResponseEntity<Book> updateBookById(
			@ApiParam(value = "Book Id for which the book will be updated", required = true) @PathVariable int bookId,
			@ApiParam(value = "Book that is to be updated", required = true) @RequestBody BookDTO bookToBeUpdated) {
		LOG.info("inside update a book /books/bookId");
		Book upadatedBook = bookService.updateBookById(bookId, bookToBeUpdated);
		return ResponseEntity.ok(upadatedBook);
	}

}
