package com.epam.bookservice;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class BookservicerestApplication {

	private static final Logger log = LogManager.getLogger(BookservicerestApplication.class);
	public static void main(String[] args) {
		log.debug("started application");
		SpringApplication.run(BookservicerestApplication.class, args);
	}

}
