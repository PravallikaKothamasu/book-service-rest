package com.epam.bookservice.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.bookservice.exceptions.NoBooksException;
import com.epam.bookservice.exceptions.NoSuchBookFound;
import com.epam.bookservice.model.Book;
import com.epam.bookservice.model.BookDTO;
import com.epam.bookservice.repository.BookRepository;

@Service
public class BookService {

	@Autowired
	BookRepository bookRepository;

	public static final Logger LOG = LogManager.getLogger(BookService.class);
	
	public List<Book> findAllBooks() {
		LOG.info("inside findAllBooks()");
		List<Book> booksList =  bookRepository.findAll();
		if (booksList.isEmpty()) {
			LOG.error("Returned empty books list: {}, throwing NoBooksException",booksList);;
			throw new NoBooksException("No books are present");
		} else {
			LOG.info("fetched and returning, books list: {}",booksList);
			return booksList;
		}
	}

	public Book getBookById(int bookId) {
		LOG.info("inside getBookById");
		return bookRepository.findById(bookId)
				.orElseThrow(() -> new NoSuchBookFound("No such book with id: " + bookId + " is found"));
	}

	public Book addBook(BookDTO bookToBeAdded) {
		LOG.info("inside addBook() and book to be added is: {}",bookToBeAdded);
		Book book = map(bookToBeAdded);
		return bookRepository.save(book);
	}

	public void deleteBookById(int bookId) {
		LOG.info("inside deleteBookById()");
		if (bookRepository.existsById(bookId)) {
			LOG.info("deleteing book with bookId: {}",bookId);
			bookRepository.deleteById(bookId);
		} else {
			LOG.error("cannot delete book with bookId: {} due to NSuchBookFoundException",bookId);
			throw new NoSuchBookFound("No book with " + bookId + " is found");
		}

	}

	public Book updateBookById(int bookId, BookDTO bookDTO) {
		LOG.info("inside updateBookById()");
		if(bookRepository.existsById(bookId)) {
		LOG.info("book to be updated is: {} and id is: {}",bookDTO,bookId);
		Book bookToBeUpdated = map(bookDTO);
		bookToBeUpdated.setBookId(bookId);
		return bookRepository.save(bookToBeUpdated);
		}else {
			LOG.info("cannot update book: {} with id: {} due to NoSuchBookFoundException ",bookDTO,bookId);
			throw new NoSuchBookFound("No book with " + bookId + " is found");
		}
	}
	
	public Book map(BookDTO bookDTO) {
		Book book = new Book();
		book.setAuthor(bookDTO.getAuthor());
		book.setTitle(bookDTO.getTitle());
		book.setCost(bookDTO.getCost());
		book.setGenre(bookDTO.getGenre());
		return book;
	}
}
