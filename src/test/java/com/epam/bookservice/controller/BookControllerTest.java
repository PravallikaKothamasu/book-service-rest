package com.epam.bookservice.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.epam.bookservice.exceptions.NoBooksException;
import com.epam.bookservice.model.Book;
import com.epam.bookservice.model.BookDTO;
import com.epam.bookservice.service.BookService;

class BookControllerTest {

	@InjectMocks
	BookController bookController;
	@Mock
	BookService bookService;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
		HttpServletRequest mockRequest = new MockHttpServletRequest();
	    ServletRequestAttributes servletRequestAttributes = new ServletRequestAttributes(mockRequest);
	    RequestContextHolder.setRequestAttributes(servletRequestAttributes);
	}

	@Test
	public void testGetListOfAllBooks() {
		List<Book> books = new ArrayList<>();
		books.add(new Book(1, "The Hobbit", "J.R.R.Tolkein", "Thriller", (long) 1000));
		books.add(new Book(2, "Slinky Malinki", "Lynley Dodd", "ScienceFiction", (long) 500));
		books.add(new Book(3, "Revolution 2020", "Chetan Bhagat", "Novel", (long) 1000));
		books.add(new Book(4, "Half-Girlfriend", "Chetan Bhagat", "Fiction", (long) 800));
		books.add(new Book(5, "Spring microservices", "John Carnell", "Technology", (long) 500));
		books.add(new Book(6, "Spring in action", "Craig Walls", "Technology", (long) 1200));
		books.add(new Book(7, "Mechanical Harry", "Bob Herr", "Science", (long) 500));
		when(bookService.findAllBooks()).thenReturn(books);
		ResponseEntity<List<Book>> responseEntity = bookController.getListOfAllBooks();
		assertEquals("200 OK", responseEntity.getStatusCode().toString());
		assertEquals(7, responseEntity.getBody().size());
	}
	
	@Test
	public void testGetListOfAllBooksWhereThereAreNoBooks() {
		when(bookService.findAllBooks()).thenThrow(NoBooksException.class);
		assertThrows(NoBooksException.class, ()->bookController.getListOfAllBooks());
	}
	
	@Test
	public void testGetBookById() {
		Book book = new Book(1, "The Hobbit", "J.R.R.Tolkein", "Thriller", (long) 1000);
		when(bookService.getBookById(1)).thenReturn(book);
		ResponseEntity<Book> responseEntity = bookController.getBookById(1);
		assertEquals("200 OK", responseEntity.getStatusCode().toString());
	}
	
	@Test
	public void testAddBook() {
		BookDTO book = new BookDTO(1, "Lost", "unknown", "comedy", (long)100);
		Book book1 = new Book(1, "Lost", "unknown", "comedy", (long)100);
		when(bookService.addBook(book)).thenReturn(book1);
		ResponseEntity<Book> response = bookController.addBook(book);
		assertEquals(201, response.getStatusCodeValue());
	}
	
	@Test
	public void testUpdateBook() {
		BookDTO book = new BookDTO(1, "Lost", "unknown", "comedy", (long)100);
		Book book1 = new Book(1, "Dont Know", "unknown", "comedy", (long)100);
		when(bookService.updateBookById(1, book)).thenReturn(book1);
		ResponseEntity<Book> responseEntity = bookController.updateBookById(1, book);
		assertEquals("200 OK", responseEntity.getStatusCode().toString());
	}
	
	@Test
	public void testDeleteBookById() {
		doNothing().when(bookService).deleteBookById(1);
		ResponseEntity<Void> response = bookController.deleteBookById(1);
		assertEquals(204, response.getStatusCodeValue());
	}

}
